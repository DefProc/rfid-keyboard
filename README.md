RFID-keyboard
=============

A 5-minute project to make using the UIDs of RFID tags a bit more human 
friendly. 

Using Adafruit PN535 RFID/NFC shield on top of an Arduino Leonardo, scanning a 
Mifare type tag (13.56MHz) will cause the UID of the tag to by typed on the 
attached computer, using the Leonardo as a USB keyboard. 

The shield profile of the RFID module means it is already fixed to the Arduino, 
and can reasonably be used as-is. But to improve practicality, top and bottom 
plates can be cut from the `case` files. The bottom plate mounts to the Arduino 
and the top plate to the RFID shield, both with 3mm plastic rivets and spacers, 
or M3 mounting hardware. 

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>

// If using the breakout or shield with I2C, define just the pins connected
// to the IRQ and reset lines.  Use the values below (2, 3) for the shield!
#define PN532_IRQ   (6)
#define PN532_RESET (3)  // Not connected by default on the NFC Shield

// Or use this line for a breakout or shield with an I2C connection:
Adafruit_PN532 nfc(PN532_IRQ, PN532_RESET);

// choose your keyboard layout (uncomment one line)
//              0,  1,  2,  3,  4,  5,  6,  7,  8,  9,  A,  B,  C,  D,   E,   F 
// Qwerty, UK
char hex[17]={ 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 100, 101, 102 }; 
// Colemak, UK
//char hex[17]={ 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 98, 99, 103, 107, 101 };
// Dvorak, UK
//char hex[17]={ 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 97, 120, 105, 104, 106, 121 };

uint8_t last_uid[] = { 0, 0, 0, 0, 0, 0, 0 };  // Buffer to store the returned UID
uint32_t last_read = 0UL;

void setup(void) {
  nfc.begin();

  uint32_t versiondata = nfc.getFirmwareVersion();
  if (! versiondata) {
    while (1); // halt
  }
  
  // configure board to read RFID tags
  nfc.SAMConfig();
  
  Keyboard.begin();
}


void loop(void) {
  uint8_t success;
  uint8_t uid[sizeof(last_uid)];  // Buffer to store the returned UID
  for (int i=0; i<sizeof(uid); i++) {
    uid[i] = last_uid[i];
  }
  uint8_t uidLength;                        // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
    
  // Wait for an ISO14443A type cards (Mifare, etc.).  When one is found
  // 'uid' will be populated with the UID, and uidLength will indicate
  // if the uid is 4 bytes (Mifare Classic) or 7 bytes (Mifare Ultralight)
  success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid, &uidLength);
  
  for (int i=0; i<uidLength; i++) {
    if (uid[i] == last_uid[i]) {
      success = false;
    }
  }
  
  if (success) {
    for (int i=0; i<uidLength; i++) {
    Keyboard.write(hex[(uid[i] >>4) & 0x0F]);
    Keyboard.write(hex[(uid[i]) & 0x0F]);
    }
    last_read = millis();
    for (int i=0; i<uidLength; i++) {
      last_uid[i] = uid[i];
    }
  }
  
  if (millis() - last_read > 1200UL) {
    for (int i=0; i<sizeof(last_uid); i++) {
      last_uid[i] = 0;
    }
  }
}
